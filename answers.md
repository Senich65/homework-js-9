1.Для створення нового HTML тегу на сторінці використовується метод 'insertAdjacentHTML'. Цей метод дозволяє вставити HTML-рядок або фрагмент HTML-коду в певне місце DOM-структури.

2.Перший параметр 'position' визначає місце, куди буде вставлений HTML-код. Є кілька можливих значень для 'position':

'beforebegin': Вставка перед початком 'element'.
'afterbegin': Вставка в середину 'element', перед всіма його дочірніми елементами.
'beforeend': Вставка в середину 'element', після всіх його дочірніх елементів.
'afterend': Вставка після 'element', на тому самому рівні з ним.

3.Щодо видалення елемента зі сторінки, ми можемо використовувати метод 'remove' або 'removeChild' для видалення елемента з DOM-структури.